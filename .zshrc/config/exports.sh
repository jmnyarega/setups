export ZSH=$HOME/.oh-my-zsh.sh
export nvm_dir="$home/.nvm"
 . "$(brew --prefix nvm)/nvm.sh"
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
export grep_options='--color=auto'
export grep_color='1;32'
export manpager="less -x"
export editor="vim"
export lscolors=gxfxcxdxbxegedabagacad
export ls_colors=gxfxcxdxbxegedabagacad
