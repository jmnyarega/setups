set rtp+=~/.vim/bundle/vundle.vim          " without .git
set runtimepath+=~/.vim/config

call vundle#rc()
runtime plugins.vim
runtime settings.vim
call vundle#end()
runtime bindings.vim
