" ################################################################################

" BASIC VIM SETTINGS 

" ###############################################################################
syntax on
filetype on
set nocompatible
set fileformats=unix,dos,mac
set background=dark
colorscheme PaperColor
set noswapfile

set tabstop=4
set shiftwidth=4
set expandtab
set smarttab

set path+=**
set wildignore=*.o,*~,*.pyc
set wildmode=list:longest,list:full
set wildmenu

set spelllang=en_gb
set encoding=utf-8
set clipboard=unnamed

set number
set t_Co=256

set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch
set completeopt=menu,longest,preview

set autochdir
set backspace=indent,eol,start

set splitbelow
set splitright

highlight Comment cterm=italic
set omnifunc=syntaxcomplete#Complete

" ################################################################################

" autoloads and autocommands 

" ###############################################################################
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif
autocmd BufEnter * lcd %:p:h
autocmd BufNewFile,BufRead *.json set ft=javascript
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

" ################################################################################

" AIRLINE SETTINGS 

" ###############################################################################
let g:Powerline_symbols='unicode'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tagbar#enabled = 0
let g:airline_theme='papercolor'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline#extensions#clock#format = '%d-%m-%Y %H:%M:%S'

" ################################################################################

" SYNTASTIC SETTINGS

" ###############################################################################
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exe = 'npm run lint --'
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_flow = 1
let g:javascript_plugin_ngdoc = 1

" ################################################################################

" NERDTree VIM SETTINGS 

" ###############################################################################
let NERDTreeShowHidden=1
